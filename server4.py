import socket
import json
from datetime import datetime
from database import Database
from user import User

db = Database()

PORT = 7070
SERVER = '127.0.0.1'
ADDR = (SERVER, PORT)

HEADER = 64
FORMAT = 'utf-8'
BUFFER = 1024

NOW = datetime.now()

server_commands = {
    "uptime": "return server life time",
    "info": "return information about server version",
    "help": "return commands list",
    "STOP": "connection closed (client and server)",
    "users": "display all users",
    "change password": "change password",
    "display credential": "display credential",
    "send message": "sending message",
    "show messages": "showing messages"
}

server_commands_admin = {
    "uptime": "return server life time",
    "info": "return information about server version",
    "help": "return commands list",
    "STOP": "connection closed (client and server)",
    "remove": "deleting any account",
    "users": "display all users",
    "change password": "change password",
    "display credential": "display credential",
    "send message": "sending message",
    "show messages": "showing messages",
    "clear messages": "removes user messages"
}

options_dict = {
    "help": server_commands,
    "info": 'Server v.1.0.0',
    "uptime": 0,
    "STOP": "STOP"
}

options_dict_admin = {
    "help": server_commands_admin,
    "info": 'Server v.1.0.0',
    "uptime": 0,
    "STOP": "STOP",
    "dupa": "dupaAdmin",
    # "users": db.update_data(),
    # "remove": db.remove_account(nic)
}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(ADDR)
    print("[SERVER] Starting...")
    s.listen()
    clientsocket, address = s.accept()
    with clientsocket:
        print(f'[SERVER] Connected by {address}')
        clientsocket.send('Click 1 to Register, or 2 to Log in'.encode(FORMAT))
        while True:
            client_recv = clientsocket.recv(BUFFER).decode(FORMAT)
            print(f'[CLIENT] {client_recv}')
            if client_recv == '2':
                clientsocket.send('Write username and password'.encode(FORMAT))
                authorization = clientsocket.recv(
                    BUFFER).decode(FORMAT).split(' ')

                if db.check_if_user_exist(authorization[0]) and db.check_if_password_correct(authorization[0], authorization[1]):
                    clientsocket.send('Correct!'.encode(FORMAT))
                    while True:
                        uptime = str(datetime.now() - NOW)
                        options_dict["uptime"] = uptime
                        client_recv = clientsocket.recv(BUFFER).decode(FORMAT)
                        print(f'[CLIENT] {client_recv}')

                        if db.check_if_admin(authorization[0], 1):
                            options_dict = options_dict_admin
                            user = User(
                                authorization[0], authorization[1], 1, box={})
                        else:
                            options_dict = options_dict
                            user = User(
                                authorization[0], authorization[1], 0, box={})

                        if client_recv in options_dict and client_recv != "STOP":
                            response = json.dumps(
                                {client_recv: options_dict[client_recv]}, indent=2)
                            clientsocket.send(response.encode(FORMAT))
                        elif client_recv == 'remove' and db.check_if_admin(authorization[0], 1):
                            clientsocket.send(
                                'Write username what you want to remove'.encode(FORMAT))
                            user_remove = clientsocket.recv(
                                BUFFER).decode(FORMAT)
                            if db.remove_account(user_remove):
                                clientsocket.send(
                                    'removed succefully'.encode(FORMAT))
                            else:
                                clientsocket.send(
                                    'removed unsuccefully'.encode(FORMAT))
                        elif client_recv == 'users':
                            clientsocket.send(json.dumps(
                                db.update_data(), indent=2).encode(FORMAT))
                        elif client_recv == 'display credential':
                            # pokazuje stare hasło dlatego ze korzysta z usera a nie bazy danych
                            db.update_data()
                            clientsocket.send(json.dumps(
                                user.display(), indent=2).encode(FORMAT))
                        elif client_recv == 'change password':
                            clientsocket.send(
                                'Write your old password and new password after spaces'.encode(FORMAT))
                            oldAndNewPassword = clientsocket.recv(
                                BUFFER).decode(FORMAT).split(' ')
                            if db.change_password(user.username, oldAndNewPassword[0], oldAndNewPassword[1]):
                                clientsocket.send(
                                    'password changed succefully'.encode(FORMAT))
                            else:
                                clientsocket.send(
                                    'password not changed'.encode(FORMAT))
                        elif client_recv == 'send message':
                            clientsocket.send(
                                'Write receiver and message content'.encode(FORMAT))
                            receiver_message = clientsocket.recv(
                                BUFFER).decode(FORMAT).split(':')
                            if db.send_message(user.username, receiver_message[0], receiver_message[1]):
                                clientsocket.send(
                                    'message sended succefully'.encode(FORMAT))
                            else:
                                clientsocket.send(
                                    'message not sended'.encode(FORMAT))
                        elif client_recv == 'show messages':
                            clientsocket.send(json.dumps(db.show_messages(
                                user.username), indent=2).encode(FORMAT))
                        elif client_recv == 'clear messages' and db.check_if_admin(authorization[0], 1):
                            clientsocket.send(
                                'Write which users you want to clear message box'.encode(FORMAT))
                            user_to_clean_message = clientsocket.recv(
                                BUFFER).decode(FORMAT)
                            if db.clear_messages(user.username, user_to_clean_message):
                                clientsocket.send(
                                    'messages clear succefully'.encode(FORMAT))
                            else:
                                clientsocket.send(
                                    'wrong login'.encode(FORMAT))
                        elif client_recv == "STOP":
                            clientsocket.send(
                                options_dict[client_recv].encode(FORMAT))
                            break
                        else:
                            clientsocket.send(
                                "command doesnt exist".encode(FORMAT))

                else:
                    clientsocket.send("Wrong login or password, choose again 1 or 2".encode(FORMAT))

            elif client_recv == '1':
                clientsocket.send(
                    'Write new username and password for register'.encode(FORMAT))
                authorization = clientsocket.recv(
                    BUFFER).decode(FORMAT).split(' ')
                try:
                    user = User(authorization[0], authorization[1], 0, box={})
                except IndexError:
                    clientsocket.send('wrong format'.encode(FORMAT))
                if db.newUser(user):
                    clientsocket.send('Now you can login'.encode(FORMAT))
                else:
                    clientsocket.send('Login already exist'.encode(FORMAT))

            else:
                clientsocket.send(
                    "Choose again 1 or 2".encode(FORMAT))
